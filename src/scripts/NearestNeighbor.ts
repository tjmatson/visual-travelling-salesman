import {Graph} from "./Graph";
import {Algorithm} from "./Salesman";
import {PointSearch} from "./PointSearch";
import {LineDrawer} from "./LineDrawer";
import {Point} from "./Point";

export class NearestNeighbor implements Algorithm {
    private notVisited: number[] = [];
    private path: number[] = [];
    private current: Point;
    private next: Point;
    private line: SVGLineElement;
    private delay: number = 500;
    private stop: boolean;

    private readonly search: PointSearch;
    private readonly drawer: LineDrawer;

    public onUpdatePath: () => void;
    public onFinished: () => void;

    constructor(public graph: Graph) {
        this.search = new PointSearch(this.graph, this.delay / 5);
        this.drawer = new LineDrawer(this.graph, "path");
    }

    setAnimateSearch(animate: boolean) : void {
        this.search.setAnimate(animate);
    }

    setDelay(delay: number) {
        this.delay = delay;
        this.drawer.setDelay(delay);
        this.search.setDelay(delay / 5);
    }
    getDelay() : number { return this.delay; }

    addPoint(x: number, y: number) : void {
        this.graph.addPoint(x,y);
    }

    private drawPath(next:  number) {
        this.path.push(next);
        this.notVisited = this.notVisited.filter(e => e !== next);
        this.next = this.graph.getPoint(next);

        this.drawer.draw(this.current, this.next).then(() => this.nextPoint());
    }

    private nextPoint() {
        if (this.stop) {
            return;
        }
        this.current = this.next;
        if (this.notVisited.length > 0) {
            this.search.search(this.current, this.notVisited).then(() => {
                this.drawPath(this.search.getNearest());
            });
        } else {
            this.drawer.draw(this.current, this.graph.getPoint(this.path[0])).then(() => {
                this.graph.setPath(this.path);
                this.onUpdatePath();
                this.onFinished();
            });
        }
    }

    solve() {
        this.init();
        for (let i = 0; i < this.graph.numPoints(); i++) {
            this.notVisited.push(i);
        }

        this.current = this.graph.getPoint(0);
        this.drawPath(0);
    }

    private init() {
        this.stop = false;
        this.graph.clearLines();
        this.notVisited = [];
        this.path = [];
        this.line = undefined;
        this.current = undefined;
        this.next = undefined;
    }

    destruct() {
        this.stop = true;
        this.search.destruct();
        this.drawer.destruct(false);
    }
}