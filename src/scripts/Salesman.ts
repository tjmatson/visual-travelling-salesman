import {Graph} from "./Graph";
import {AntColony} from "./AntColony";
import {TwoOpt} from "./TwoOpt";
import {NearestNeighbor} from "./NearestNeighbor";
import {FarthestInsertion} from "./FarthestInsertion";

export interface Algorithm {
    addPoint(x: number, y: number) : void;
    setDelay(delay: number) : void;
    solve();
    destruct();
    onUpdatePath: () => void;
    onFinished: () => void;
}

class TravellingSalesman {
    private startStopIcon: SVGPolygonElement;
    // Controls
    private htmlStart: HTMLButtonElement;
    private htmlAlgorithm: HTMLSelectElement;
    private htmlSpeed: HTMLInputElement;
    private htmlClear: HTMLButtonElement;
    private htmlRandom: HTMLButtonElement;
    private htmlRandomAmount: HTMLInputElement;

    private htmlIterations: HTMLInputElement;
    private htmlAnts: HTMLInputElement;
    private htmlAnimateAnts: HTMLInputElement;

    private htmlAnimateSearch: HTMLInputElement;
    private htmlAnimateInsert: HTMLInputElement;

    private htmlDistance: HTMLSpanElement;

    private algorithm: Algorithm;

    private running: boolean = false;

    constructor(public graph: Graph) {
        this.startStopIcon = <SVGPolygonElement>document.querySelector("#start-stop-icon");
        this.htmlAlgorithm = <HTMLSelectElement>document.querySelector("#algorithm");
        this.htmlStart = <HTMLButtonElement>document.querySelector("#start");
        this.htmlIterations = <HTMLInputElement>document.querySelector("#iterations");
        this.htmlSpeed = <HTMLInputElement>document.querySelector("#speed");
        this.htmlClear = <HTMLButtonElement>document.querySelector("#clear");
        this.htmlRandom = <HTMLButtonElement>document.querySelector("#random");
        this.htmlRandomAmount = <HTMLInputElement>document.querySelector("#random-input");

        this.htmlAnts = <HTMLInputElement>document.querySelector("#ants");
        this.htmlAnimateAnts = <HTMLInputElement>document.querySelector("#animateAnts");

        this.htmlAnimateSearch = <HTMLInputElement>document.querySelector("#animateSearch");
        this.htmlAnimateInsert = <HTMLInputElement>document.querySelector("#animateInsert");

        this.htmlDistance = <HTMLSpanElement>document.querySelector("#distance");

        this.switchAlgorithm(this.htmlAlgorithm.value);

        this.htmlAlgorithm.addEventListener("change", () =>
            this.switchAlgorithm(this.htmlAlgorithm.value)
        );

        this.htmlStart.addEventListener("click", () => {
            this.startStop();
        });

        this.htmlSpeed.addEventListener("input", () =>
            this.setSpeed(parseInt(this.htmlSpeed.value))
        );

        this.htmlAnimateAnts.addEventListener("change", () => {
            if (this.algorithm instanceof AntColony) {
                this.algorithm.setAnimateAnts(this.htmlAnimateAnts.checked);
            }
        });

        this.htmlAnimateSearch.addEventListener("change", () => {
            this.setAnimateSearch();
        });

        this.htmlAnimateInsert.addEventListener("change", () => {
            if (this.algorithm instanceof FarthestInsertion) {
                this.algorithm.setAnimateInsert(this.htmlAnimateInsert.checked);
            }
        });

        this.htmlClear.addEventListener("click", () => {
            this.htmlDistance.innerText = "0";
            this.destruct();
            this.graph.clearAll();
        });

        this.htmlRandom.addEventListener("click", () => {
            this.destruct();
            this.generateRandom(Number.parseInt(this.htmlRandomAmount.value));
        });
    }

    startStop() {
        if (this.running) {
            this.destruct();
        } else {
            this.solve();
        }
        this.updateStartStopIcon();
    }

    updateStartStopIcon() {
        if (this.running) {
            // Stop button
            this.startStopIcon.setAttributeNS(null, "points", "0,0 20,0 20,20 0,20")
        } else {
            // Play button
            this.startStopIcon.setAttributeNS(null, "points", "0,0 20,10 0,20")
        }
    }

    generateRandom(num: number) {
        this.htmlDistance.innerText = "0";
        this.algorithm.destruct();
        this.graph.generateRandom(num);
    }

    setSpeed(speed: number) {
        this.algorithm.setDelay(2000 * (1 - speed/1000));
    }

    switchAlgorithm(name: string) {
        // Hide ant colony controls
        for (let control of Array.from(document.getElementsByClassName("ant-control"))) {
            (control as HTMLElement).classList.add("hidden");
        }
        document.querySelector(".animate-search").classList.add("hidden");
        document.querySelector(".animate-insert").classList.add("hidden");
        switch (name) {
            case "nearestNeighbor":
                this.setAlgorithm(new NearestNeighbor(this.graph));
                this.showSearchOption();
                break;
            case "2opt":
                this.setAlgorithm(new TwoOpt(this.graph));
                break;
            case "antColony":
                this.setAntColony();
                break;
            case "farthestInsertion":
                this.setAlgorithm(new FarthestInsertion(this.graph));
                document.querySelector(".animate-insert").classList.remove("hidden");
                this.showSearchOption();
                break;
        }
    }

    setAntColony() {
        // Show ant colony controls
        for (let control of Array.from(document.getElementsByClassName("ant-control"))) {
            (control as HTMLElement).classList.remove("hidden");
        }
        this.setAlgorithm(new AntColony(this.graph));
    }

    showSearchOption() {
        document.querySelector(".animate-search").classList.remove("hidden");
    }

    setAnimateSearch() {
        if (this.algorithm instanceof FarthestInsertion) {
            this.algorithm.setAnimateSearch(this.htmlAnimateSearch.checked);
        } else if (this.algorithm instanceof NearestNeighbor) {
            this.algorithm.setAnimateSearch(this.htmlAnimateSearch.checked);
        }
    }

    setAlgorithm(algorithm: Algorithm) {
        this.destruct();
        this.algorithm = algorithm;
    }

    destruct() {
        this.running = false;
        if (this.algorithm) {
            this.algorithm.destruct();
        }
    }

    solve() {
        this.destruct();
        this.graph.clearLines();
        this.graph.clearCanvas();
        this.setSpeed(parseInt(this.htmlSpeed.value));

        // Update distance display when algorithm calls back
        this.algorithm.onUpdatePath = () => {
            this.htmlDistance.innerText = this.graph.pathDistance(this.graph.getPath()).toFixed(1);
        };
        this.algorithm.onFinished = () => {
            this.running = false;
            this.updateStartStopIcon();
        };

        // Algorithm specific parameters
        if (this.algorithm instanceof AntColony) {
            this.algorithm.setNumAnts(parseInt(this.htmlAnts.value));
            this.algorithm.setAnimateAnts(this.htmlAnimateAnts.checked);
            this.algorithm.setMaxIterations(parseInt(this.htmlIterations.value));
        } else if (this.algorithm instanceof FarthestInsertion) {
            this.algorithm.setAnimateInsert(this.htmlAnimateInsert.checked);
        }
        this.setAnimateSearch();

        this.running = true;
        this.algorithm.solve();
    }
}

let canvas = <HTMLCanvasElement>document.getElementById("canvas");
let svg = <SVGSVGElement><any>document.getElementById("svg");

let graph = new Graph(canvas, svg);
let ts = new TravellingSalesman(graph);
ts.generateRandom(10);

