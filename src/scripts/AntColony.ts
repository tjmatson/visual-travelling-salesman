import {Graph} from "./Graph";
import {Ant} from "./Ant";
import {Algorithm} from "./Salesman";
import {Tools} from "./Tools";
import {Point} from "./Point";

export class AntColony implements Algorithm {
    private edges: number[][] = [];
    private ants: AntElement[] = [];
    private antCounter: number;
    private numAnts: number;

    private shortestTrail: number[];
    private shortestLength: number;
    private maximum: number = 0; // Edge with maximum probability. Used to scale edge's opacity between 0 to 1 when drawing.

    readonly antGroup: Element;

    public onUpdatePath: () => void;
    public onFinished: () => void;
    private stop : boolean;

    // Constants
    Q: number = 1000;
    evaporation: number = 0.5;
    alpha: number = 1;
    beta: number = 10;

    private moves: number = 0; // Current number of moves in constructing trail.
    private iterations: number = 0; // Current number of trails that have been generated.
    private maxIterations: number = 100; // Maximum number of trails to generate.
    private animateAnts: boolean = true;
    private hidden: boolean = false; // Whether the ant elements are currently hidden
    private delay: number = 500; // Time each movement animation takes in milliseconds.

    /**
     * Adapted from https://www.baeldung.com/java-ant-colony-optimization
     */
    constructor(public graph: Graph) {
        this.antGroup = this.graph.addOrGetGroup("ants");
        this.antGroup.classList.add("antGroup");
    }

    setDelay(delay: number) {
        this.delay = delay;
        for (let ant of this.ants) {
            ant.setDelay(delay);
        }
    }
    getDelay() { return this.delay; }

    setMaxIterations(maxIterations: number) { this.maxIterations = maxIterations; }
    getMaxIterations() : number { return this.maxIterations; }

    setAnimateAnts(animateAnts: boolean) { this.animateAnts = animateAnts; }
    getAnimateAnts() : boolean { return this.animateAnts; }

    setNumAnts(numAnts: number) { this.numAnts = numAnts; }
    getNumAnts() : number { return this.numAnts; }

    addPoint(x: number, y: number) {
        this.graph.addPoint(x, y);
        this.createEdges();
    }

    private addAnt() {
        let ant: AntElement = new AntElement(this.graph, this);
        this.ants.push(ant);
    }

    private createEdges() {
        this.edges = [];
        this.graph.clearLines();
        for (let i = 0; i < this.graph.numPoints() - 1; i++) {
            this.edges.push([]);
            for (let j = i + 1; j < this.graph.numPoints(); j++) {
                this.edges[i].push(1);
            }
        }
    }

    /**
     * Get the edge between two points based on the point's indices in the array.
     * @param a Index of a point in the array
     * @param b Index of a point in the array
     * @return the edge between those points.
     */
    public getEdge(a: number,  b: number) {
        [a, b] = Tools.order(a, b);
        return this.edges[a][b - (a + 1)];
    }

    private setEdge(a: number, b: number, num: number) {
        [a, b] = Tools.order(a, b);
        this.edges[a][b - (a + 1)] = num;
    }

    private drawEdges() {
        this.graph.clearCanvas();
        let context = this.graph.canvasContext;
        context.lineWidth = 2;
        for (let i = 0; i < this.graph.numPoints() - 1; i++) {
            for (let j = i + 1; j < this.graph.numPoints(); j++) {
                let point1: Point = this.graph.getPoint(i);
                let point2: Point = this.graph.getPoint(j);

                context.strokeStyle = `rgba(0, 0, 255, ${this.edges[i][j - (i + 1)] / this.maximum})`;
                context.beginPath();
                context.moveTo(point1.x, point1.y);
                context.lineTo(point2.x, point2.y);
                context.stroke();
            }
        }
    }

    private updateEdges() {
        this.maximum = 0;

        // Evaporation
        for (let i = 0; i < this.edges.length; i++) {
            for (let j = 0; j < this.edges[i].length; j++) {
                this.edges[i][j] *= (1 - this.evaporation);
            }
        }

        for (let antElement of this.ants) {
            let ant = antElement.ant;
            let contribution: number = this.Q / this.graph.pathDistance(ant.trail);
            // Add pheromones to edges in path
            for (let point = 0; point < ant.trail.length - 1; point++) {
                let p = this.getEdge(ant.trail[point], ant.trail[point + 1]) + contribution;
                if (p > this.maximum) {
                    this.maximum = p;
                }
                this.setEdge(ant.trail[point], ant.trail[point + 1], p);
            }

            // Connect end and beginning points.
            let p = this.getEdge(ant.trail[0], ant.trail[ant.trail.length - 1]) + contribution;
            if (p > this.maximum) { this.maximum = p; }
            this.setEdge(ant.trail[0], ant.trail[ant.trail.length - 1], p);
        }
    }

    private updateShortestTrail() {
        if (!this.shortestTrail) {
            this.shortestTrail = this.ants[0].ant.trail;
            this.shortestLength = this.graph.pathDistance(this.ants[0].ant.trail);
        }
        for (let antElement of this.ants) {
            let ant = antElement.ant;
            let length = this.graph.pathDistance(ant.trail);
            if (length < this.shortestLength) {
                this.shortestTrail = ant.trail;
                this.shortestLength = length;
            }
        }
        this.graph.setPath(this.shortestTrail);
        this.onUpdatePath();
    }

    private startNextTrail() {
        if (this.stop) {
            return;
        }
        this.moves = 0;
        if (this.iterations < this.maxIterations) {
            for (let ant of this.ants) {
                ant.forgetTrail();
            }
            this.moveAnts();
        } else {
            this.clearAntElements();
            this.drawShortestPath();
            this.onFinished();
        }
        this.iterations++;
    }

    /**
     * Move all ants to every point.
     */
    private moveAnts() {
        if (this.stop) {
            return;
        }
        if (this.moves < this.graph.numPoints()) {
            this.moves++;
            if (this.animateAnts) {
                if (this.hidden) {
                    this.ants.forEach(ant => ant.show());
                    this.hidden = false;
                }

                this.ants.forEach(ant => ant.nextPoint(true) );

                if (this.moves != 1) {
                    Promise.all(this.ants.map(ant => ant.movementTransition())).then(() => {
                        this.moveAnts();
                    });
                } else {
                    this.moveAnts();
                }
            } else {
                if (!this.hidden) {
                    this.ants.forEach(ant => ant.hide());
                    this.hidden = true;
                }

                this.ants.forEach(ant => ant.nextPoint(false));
                // This should always be the last statement in this path to allow for tail-call optimization.
                this.moveAnts();
            }
        } else {
            this.updateAll();
            // Add delay between trails rather than movement of ants if ants aren't being animated.
            if (!this.animateAnts) {
                setTimeout(() => this.startNextTrail(), this.delay)
            } else {
                this.startNextTrail()
            }
        }
    }

    private clearAntElements() {
        this.graph.clearGroup("ants");
    }

    /**
     * Update and draw edge probabilities and get shortest trail.
     */
    private updateAll() {
        this.updateEdges();
        this.drawEdges();
        this.updateShortestTrail();
    }

    private drawShortestPath() {
        this.graph.clearLines();
        for (let i = 0; i < this.shortestTrail.length - 1; i++) {
            let line = Tools.createLine("black", "2");
            let point1: Point = this.graph.getPoint(this.shortestTrail[i]);
            let point2: Point = this.graph.getPoint(this.shortestTrail[i + 1]);

            Tools.moveLine(line, point1.x, point1.y, point2.x, point2.y);

            this.graph.getGroup("path").append(line);
        }

        let line = Tools.createLine("black", "2");
        let point1: Point = this.graph.getPoint(this.shortestTrail[0]);
        let point2: Point = this.graph.getPoint(this.shortestTrail[this.shortestTrail.length - 1]);

        Tools.moveLine(line, point1.x, point1.y, point2.x, point2.y);

        this.graph.getGroup("path").append(line);
    }

    solve() {
        this.init();
        this.graph.clearLines();
        this.graph.clearCanvas();

        this.startNextTrail();
    }

    init() {
        this.moves = 0;
        this.iterations = 0;
        this.ants = [];
        this.antCounter = 0;
        this.shortestTrail = null;
        this.shortestLength = null;
        this.hidden = false;
        this.stop = false;

        this.createEdges();
        this.clearAntElements();

        for (let i = 0; i < this.numAnts; i++) {
            this.addAnt();
        }
    }

    destruct() {
        this.stop = true;
        this.graph.clearCanvas();
        for (let ant of this.ants) {
            ant.destruct();
        }
    }
}

class AntElement {
    private readonly antElement: SVGCircleElement;
    public ant: Ant;
    private start: Point;
    private end: Point;
    private startTime: number;
    private delay: number;
    private stop : boolean;

    constructor(private graph: Graph, private colony: AntColony) {
        this.antElement = document.createElementNS("http://www.w3.org/2000/svg", "circle");
        this.antElement.setAttributeNS(null, "r", "5");
        this.antElement.setAttributeNS(null, "fill", "orange");
        colony.antGroup.append(this.antElement);
        this.ant = new Ant(colony);
        this.setDelay(colony.getDelay());
    }

    hide() {
        this.antElement.style.visibility = "hidden";
    }

    show() {
        this.antElement.style.visibility = "visible";
    }

    setDelay(delay: number) {
        // Add random +-20% delay to avoid uniform movement
        this.delay = delay + (Math.random() * delay * 0.4) - delay * 0.2;
    }

    forgetTrail() {
        this.start = undefined;
        this.end = undefined;
        this.ant.forgetTrail();
    }

    nextPoint(animate: boolean) {
        this.stop = false;
        if (this.end){
            this.start = this.end;
            this.end = this.graph.getPoint(this.ant.visitPoint());

            if (animate) this.startTime = Date.now();
        } else {
            this.end = this.graph.getPoint(this.ant.visitPoint());
        }
    }

    movementTransition() {
        return new Promise((resolve, reject) => {
            let frame = () => {
                if (this.stop) {
                    reject();
                }
                let percent = Tools.ease(Math.min((Date.now() - this.startTime) / this.delay, 1));
                let x = (this.end.x - this.start.x) * percent + this.start.x;
                let y = (this.end.y - this.start.y) * percent + this.start.y;
                this.antElement.setAttributeNS(null, "cx", x.toString());
                this.antElement.setAttributeNS(null, "cy", y.toString());

                if (Date.now() - this.startTime >= this.delay)  {
                    resolve();
                } else {
                    requestAnimationFrame(() => frame());
                }
            };

            requestAnimationFrame(() => frame());
        })
    }

    destruct() {
        this.stop = true;
    }
}
