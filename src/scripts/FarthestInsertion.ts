import {Algorithm} from "./Salesman";
import {Graph} from "./Graph";
import {Tools} from "./Tools";
import {Connection, Line} from "./TwoOpt";
import {PointSearch} from "./PointSearch";
import {LineDrawer} from "./LineDrawer";
import {Coordinate} from "./Point";

export class FarthestInsertion implements Algorithm {
    private delay: number = 500;
    private path: number[];
    private notVisited: number[];
    private current: number;
    private distanceToTour: Map<number, number>;

    private smallestChangeDistance: number;
    private smallestChange: number;
    private index: number;

    private animateInsert: boolean = true;
    private stop: boolean;

    private readonly search: PointSearch;
    private readonly drawer: LineDrawer;
    private drawerDelayFactor: number = 5;
    private searcherDelayFactor: number = 2;

    public onUpdatePath: () => void;
    public onFinished: () => void;

    constructor(private graph: Graph) {
        this.search = new PointSearch(graph, this.delay / this.searcherDelayFactor);
        this.search.setShowMarker(false);

        this.drawer = new LineDrawer(graph, "lines");
        this.drawer.setDelay(this.delay / this.drawerDelayFactor);
        this.drawer.setStroke("blue");
        this.drawer.setLineProperty("stroke-dasharray", "10,10");
    }

    setAnimateSearch(animate: boolean) : void {
        this.search.setAnimate(animate);
    }

    setAnimateInsert(animate: boolean) : void {
        this.animateInsert = animate;
    }

    setDelay(delay: number): void {
        this.delay = delay;
        this.search.setDelay(delay / this.drawerDelayFactor);
        this.drawer.setDelay(this.delay / this.searcherDelayFactor);
    }
    getDelay() : number { return this.delay; }

    addPoint(x: number, y: number) : void {
        this.graph.addPoint(x, y);
    }

    private nextPoint() {
        this.smallestChangeDistance = -1;
        let farthest;
        let farthestDistance = -1;
        // Find the distance from every point that isn't in the sub-tour to the point that has just been added.
        for (let [point, distance] of this.search.getDistances()) {
            // Adding a point to the sub-tour will always result in the distance from a point to the sub-tour
            // remaining the same or reducing. Update distance if it has been reduced.
            if (!this.distanceToTour.has(point) || distance < this.distanceToTour.get(point)) {
                this.distanceToTour.set(point, distance);
            }
            distance = this.distanceToTour.get(point);
            if (distance > farthestDistance || farthestDistance == -1) {
                farthest = point;
                farthestDistance = distance;
            }
        }

        this.current = farthest;
        this.notVisited = this.notVisited.filter(e => e != this.current);
        if (this.path.length == 1) {
            // Add point to sub-tour when there's only one other point in the sub-tour.
            this.path.push(this.current);

            let point1 = this.graph.getPoint(this.path[0]);
            let point2 = this.graph.getPoint(this.current);
            let drawer = new LineDrawer(this.graph, "path");
            drawer.setDelay(this.delay);

            drawer.draw(point1, point2).then(() => {
                this.search.search(this.graph.getPoint(this.current), this.notVisited).then(() => {
                    this.nextPoint();
                });
            });
        } else {
            this.findMinInsert();
        }
    }

    private findMinInsert() {
        if (this.stop) {
            return;
        }
        // Iterate through every pair of points in the sub-tour.
        if (this.index < this.path.length - 1) {
            let point1 = this.graph.getPoint(this.path[this.index]);
            let point2 = this.graph.getPoint(this.path[this.index + 1]);
            let newPoint = this.graph.getPoint(this.current);

            // Find the distance between two points and compare it to the distance if the new point
            // is inserted between them. Choose the points with the minimal change in distance.
            let originalDistance = point1.getDistance(point2);
            let insertionDistance = point1.getDistance(newPoint) + point2.getDistance(newPoint);
            if (insertionDistance - originalDistance < this.smallestChangeDistance || this.smallestChangeDistance == -1) {
                this.smallestChangeDistance = insertionDistance - originalDistance;
                this.smallestChange = this.index;
            }

            if (this.animateInsert) {
                Promise.all([
                    this.drawer.draw(point1, newPoint),
                    this.drawer.draw(point2, newPoint)
                ]).then(() => {
                    this.drawer.clear();
                    this.index++;
                    this.findMinInsert();
                })
            } else {
                this.index++;
                this.findMinInsert();
            }
        } else {
            this.insertTransition();
        }
    }

    insertTransition() {
        this.index = 0;
        this.graph.clearLines();

        // Split path into sections before and after the point to be inserted and redraw those sections,
        // leaving a gap between the sections.
        let section1 = this.path.slice(0, this.smallestChange + 1);
        let section2 = this.path.slice(this.smallestChange + 1, this.path.length);
        for (let section of [section1, section2]) {
            for (let i = 0; i < section.length - 1; i++) {
                let point1 = this.graph.getPoint(section[i]);
                let point2 = this.graph.getPoint(section[i + 1]);
                let line = Tools.createLine("black", "2");
                Tools.moveLine(line, point1.x, point1.y, point2.x, point2.y);
                this.graph.getGroup("path").append(line);
            }
        }

        // Draw two lines that connect the two sections together that will attach to the point to be inserted.
        let point1 = this.graph.getPoint(this.path[this.smallestChange]);
        let point2 = this.graph.getPoint(this.path[this.smallestChange + 1]);
        let mid = new Coordinate((point1.x + point2.x)/2, (point1.y + point2.y)/2);
        let side1 = new Line(new Connection(point1, mid, this.graph));
        let side2 = new Line(new Connection(point2, mid, this.graph));

        side1.setDelay(this.delay);
        side2.setDelay(this.delay);
        let p1 = side1.pivot(new Connection(point1, this.current, this.graph));
        let p2 = side2.pivot(new Connection(point2, this.current, this.graph));

        this.path.splice(this.smallestChange + 1, 0, this.current);

        Promise.all([p1, p2]).then(() => {
            if (this.notVisited.length > 0) {
                this.search.search(this.graph.getPoint(this.current), this.notVisited).then(() => {
                    this.nextPoint();
                })
            } else {
                let drawer = new LineDrawer(this.graph, "path");
                drawer.setDelay(this.delay);
                drawer.draw(this.graph.getPoint(this.path[this.path.length - 1]), this.graph.getPoint(0)).then(() => {
                    this.graph.setPath(this.path);
                    this.onUpdatePath();
                    this.onFinished();
                });
            }
        });
    }

    /**
     * solve : Add a starting point to sub-tour and find distances of all other points
     * -> nextPoint : Find point farthest from starting point and add it to sub-tour
     * -> nextPoint : Find point farthest from any other point in the sub-tour
     * -> findMinInsert : Find the edge with the minimal cost change when this new point is inserted between it.
     * -> insertTransition : Animate inserting point into the sub-tour
     * -> nextPoint : Repeat until all points have been added to sub-tour
     */
    solve() {
        this.init();

        this.search.search(this.graph.getPoint(this.current), this.notVisited).then(() => {
            this.nextPoint();
        });
    }

    private init() {
        this.current = 0;
        this.index = 0;
        this.notVisited = [];
        this.smallestChangeDistance = -1;
        this.stop = false;
        this.path = [];
        for (let i = 1; i < this.graph.numPoints(); i++) {
            this.notVisited.push(i);
        }
        this.path.push(this.current);
        this.distanceToTour = new Map<number, number>();
    }

    destruct() {
        this.stop = true;
        this.drawer.destruct();
        this.search.destruct();
    }
}