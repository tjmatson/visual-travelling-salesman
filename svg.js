let svg = null;

let nodes = new Map();
let counter = 0;

class Point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}

Point.prototype.distance = function(point) {
    
};

class Node {
    constructor(x, y) {
        this.connections = [];
        this.x = x;
        this.y = y;

        this.node = document.createElementNS("http://www.w3.org/2000/svg", "circle");
        this.node.setAttributeNS(null, "cx", x.toString());
        this.node.setAttributeNS(null, "cy", y.toString());
        this.node.setAttributeNS(null, "r", "10");
        this.node.setAttributeNS(null, "id", "node" + counter);
        this.node.classList.add("node");
        nodes.set("node" + counter, this);
        counter++;

        svg.appendChild(this.node);
    }
}

Node.prototype.connect = function(node) {
    let line = document.createElementNS("http://www.w3.org/2000/svg", "line");
    line.setAttributeNS(null, "x1", this.x.toString());
    line.setAttributeNS(null, "y1", this.y.toString());
    line.setAttributeNS(null, "x2", node.x.toString());
    line.setAttributeNS(null, "y2", node.y.toString());
    line.setAttributeNS(null, "stroke", "black");
    svg.appendChild(line);

    this.connections.push({line: line, side: 1, other: node});
    node.connections.push({line: line, side: 2, other: this});
};

Node.prototype.move = function(x, y) {
    this.x = x;
    this.y = y;
    this.node.setAttributeNS(null, "cx", x.toString());
    this.node.setAttributeNS(null, "cy", y.toString());

    for (let pair of this.connections) {
        pair.line.setAttributeNS(null, "x" + pair.side, x.toString());
        pair.line.setAttributeNS(null, "y" + pair.side, y.toString());
    }
};

function init() {
    svg = document.getElementById("svg");
    makeNodesDraggable();
    let n1 = new Node(10, 50);
    let n2 = new Node(150, 100);
    let n3 = new Node(100, 200);
    let n4 = new Node(50, 150);
    n1.connect(n2);
    n2.connect(n3);
    n3.connect(n4);
    n4.connect(n1);
    n1.connect(n3);
    n2.connect(n4);
}

function getMousePosition(event) {
    let CTM = svg.getScreenCTM();
    return {
        x: (event.clientX - CTM.e) / CTM.a,
        y: (event.clientY - CTM.f) / CTM.d,
    };
}

function makeNodesDraggable() {
    svg.addEventListener("mousedown", start);
    svg.addEventListener("mousemove", drag);
    svg.addEventListener("mouseup", end);
    svg.addEventListener("mouseleave", end);

    let element = null;
    let offset = {};

    function start(event) {
        if (event.target.classList.contains("node")) {
            element = event.target;
            let coords = getMousePosition(event);
            offset.x = parseFloat(element.getAttributeNS(null, "cx")) - coords.x;
            offset.y = parseFloat(element.getAttributeNS(null, "cy")) - coords.y;
        }
    }

    function drag(event) {
        if (element !== null) {
            event.preventDefault();
            let coords = getMousePosition(event);
            let node = nodes.get(element.id);
            node.move(coords.x + offset.x, coords.y + offset.y);
        }
    }

    function end() {
        element = null;
    }
}