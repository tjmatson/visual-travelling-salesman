import {Tools} from "./Tools";
import {Graph} from "./Graph";
import {Point} from "./Point";

export class LineDrawer {
    private delay: number = 500;
    private stop: boolean;

    private groupName;
    private group: SVGGElement;

    private stroke: string = "black";
    private width: string = "2";
    private readonly lineProperties: Map<string, string>;

    constructor(private graph: Graph, group: string) {
        this.lineProperties = new Map<string, string>();
        this.group = graph.addOrGetGroup(group);
        this.groupName = group;
    }

    setDelay(delay: number) {
        this.delay = delay;
    }

    getDelay() {
        return this.delay;
    }

    setStroke(stroke: string) {
        this.stroke = stroke;
    }

    getStroke() {
        return this.stroke;
    }

    setWidth(width: string) {
        this.width = width;
    }

    getWidth() {
        return this.width;
    }

    setLineProperty(property: string, value: string) {
        this.lineProperties.set(property, value);
    }

    getLineProperty(property: string) {
        return this.lineProperties.get(property);
    }

    setGroup(name: string) {
        this.groupName = name;
        this.group = this.graph.addOrGetGroup(name);
    }

    getGroup(): string {
        return this.groupName;
    }

    clear() {
        this.graph.clearGroup(this.groupName);
    }

    draw(beginning: Point, end: Point): Promise<SVGLineElement> {
        this.stop = false;

        let line = Tools.createLine(this.stroke, this.width);
        for (let [property, value] of this.lineProperties) {
            line.setAttributeNS(null, property, value);
        }
        Tools.moveLine(line, beginning.x, beginning.y, beginning.x, beginning.y);
        this.group.append(line);

        return this.transition(line, beginning, end);
    }

    private transition(line: SVGLineElement, beginning: Point, end: Point): Promise<SVGLineElement> {
        return new Promise((resolve, reject) => {
            let frame = () => {
                if (this.stop) {
                    return reject();
                }
                let percent = Tools.ease(Math.min((Date.now() - start) / this.delay, 1));
                let x = (end.x - beginning.x) * percent + beginning.x;
                let y = (end.y - beginning.y) * percent + beginning.y;
                line.setAttributeNS(null, "x2", x.toString());
                line.setAttributeNS(null, "y2", y.toString());

                if (Date.now() - start < this.delay) {
                    requestAnimationFrame(() => frame());
                } else {
                    resolve(line);
                }
            };

            let start = Date.now();
            frame();
        });
    }

    destruct(clear?: boolean) {
        this.stop = true;
        if (clear == undefined || clear) this.graph.clearGroup(this.groupName);
    }
}