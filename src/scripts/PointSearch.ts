import {Tools} from "./Tools";
import {Graph} from "./Graph";
import {Point} from "./Point";

export class PointSearch {
    private point: Point;
    private inclusions: number[];

    private nearest: number; // Index of the nearest point
    private nearestDistance: number;
    private farthest: number; // Index of the farthest point
    private farthestDistance: number;

    private distances: Map<number, number>; // Distance of each point from target point
    private currentPoint: number = 0;
    private stop: boolean;

    private readonly searchLine: SVGLineElement;
    private readonly markerLine: SVGLineElement;
    private searchNearest: boolean = true;
    private showMarker: boolean = true;
    private animate: boolean = true;
    private readonly group: SVGGElement;
    private readonly groupName = "searchLines";

    constructor(private graph: Graph, private delay: number) {
        this.searchLine = Tools.createLine("blue", "2");
        this.markerLine = Tools.createLine("red", "2");
        this.group = this.graph.addOrGetGroup(this.groupName);
    }

    setSearchNearest() {
        this.searchNearest = true;
    }

    setSearchFarthest() {
        this.searchNearest = false;
    }

    setShowMarker(showMarker: boolean) {
        this.showMarker = showMarker;
    }

    setAnimate(animate: boolean) {
        this.animate = animate;
    }

    getNearest() {
        return this.nearest;
    }

    getFarthest() {
        return this.farthestDistance;
    }

    setDelay(delay: number) {
        this.delay = delay;
    }

    getDelay(): number {
        return this.delay;
    }

    getDistances(): Map<number, number> {
        return this.distances;
    }

    search(point: Point, inclusions: number[]): Promise<void> {
        this.point = point;
        this.inclusions = inclusions;
        this.currentPoint = 0;
        this.nearestDistance = -1;
        this.nearest = null;
        this.farthestDistance = -1;
        this.farthest = undefined;
        this.distances = new Map<number, number>();
        this.stop = false;

        Tools.moveLine(this.markerLine, 0, 0, 0, 0);
        Tools.moveLine(this.searchLine, 0, 0, 0, 0);
        this.group.append(this.searchLine);
        this.group.append(this.markerLine);

        return this.searchPoints();
    }

    private searchPoints(): Promise<void> {
        return new Promise((resolve, reject) => {
            let frame = () => {
                if (this.stop) {
                    return reject();
                }
                if (this.currentPoint >= this.graph.numPoints()) {
                    this.graph.clearGroup(this.groupName);
                    return resolve();
                }
                let timeout = (this.animate) ? this.delay : 0;
                if (this.inclusions.indexOf(this.currentPoint) >= 0) {
                    let point = this.graph.getPoint(this.currentPoint);
                    let distance = this.point.getDistance(point);
                    this.distances.set(this.currentPoint, distance);

                    if (distance < this.nearestDistance || this.nearestDistance == -1) {
                        this.nearest = this.currentPoint;
                        this.nearestDistance = distance;
                    }
                    if (distance > this.farthestDistance || this.farthestDistance == -1) {
                        this.farthest = this.currentPoint;
                        this.farthestDistance = distance;
                    }

                    if (this.animate) {
                        this.drawLines(point);
                    }
                } else {
                    timeout = 0;
                }

                this.currentPoint++;
                if (timeout == 0) {
                    frame();
                } else {
                    setTimeout(() => frame(), timeout);
                }
            };

            frame();
        });
    }

    drawLines(point: Point) {
        if (this.searchNearest && this.showMarker) {
            let nearestPoint = this.graph.getPoint(this.nearest);
            Tools.moveLine(this.markerLine, this.point.x, this.point.y, nearestPoint.x, nearestPoint.y);
        } else if (this.showMarker) {
            let farthestPoint = this.graph.getPoint(this.farthest);
            Tools.moveLine(this.markerLine, this.point.x, this.point.y, farthestPoint.x, farthestPoint.y);
        }

        Tools.moveLine(this.searchLine, this.point.x, this.point.y, point.x, point.y);
    }

    destruct() {
        this.stop = true;
        this.graph.clearGroup(this.groupName);
    }
}