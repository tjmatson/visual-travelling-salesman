import {Algorithm} from './Salesman'
import {Graph} from "./Graph";
import {Tools} from "./Tools";
import {Coordinate} from "./Point";

export class TwoOpt implements Algorithm {
    private path: number[];
    private lines: Map<string, Line> = new Map<string, Line>();

    // Number of tries with no successful swap before ending
    private maxTries: number = 500;
    private tries: number;

    private delay: number = 500;

    private iCurrent: number = 0;
    private kCurrent: number = 0;

    private stop: boolean;

    public onUpdatePath: () => void;
    public onFinished: () => void;

    constructor(public graph: Graph) {
    }

    addPoint(x: number, y: number) {
        this.graph.addPoint(x, y);
    }

    setDelay(delay: number) {
        this.delay = delay;
        this.lines.forEach(line => line.setDelay(delay));
    }
    getDelay() { return this.delay; }

    setMaxTries(maxTries: number) { this.maxTries = maxTries; }
    getMaxTries() { return this.maxTries; }

    /**
     * Basic 2-opt swap algorithm via reversing a section of the path.
     */
    private swap(i: number, k: number) : number[] {
        let newPath = this.path.slice(0, i);
        newPath = newPath.concat(this.path.slice(i, k + 1).reverse());
        newPath = newPath.concat(this.path.slice(k + 1, this.path.length));
        return newPath;
    }

    private swapTransition(path: number[]) {
        if (this.stop) {
            return;
        }
        let promises: Promise<void>[] = [];
        // Sort so that start < end
        let [start, end] = [this.iCurrent, this.kCurrent];

        let swapLine = (x: number, y: number) => {
            // Get the line that forms the connection at the current index and swap it to the new connection
            let connection = new Connection(this.path[x], this.path[y], this.graph);
            let line = this.lines.get(connection.toString());
            let swap = new Connection(path[x], path[y], this.graph);
            promises.push(line.pivot(swap));
            return {old: connection.toString(), new: swap.toString(), line: line};
        };

        // No swap is made when indices are equal
        let swap1 = swapLine(start, Tools.mod(start - 1, this.path.length));
        let swap2 = swapLine(end, Tools.mod(end + 1, this.path.length));
        // Update key values to reflect new connections.
        // It's possible for the swaps to conflict with each other, so wait until after to update map values.
        // e.g. [1, 2, 3, 4] => [1, 4, 3, 2]
        // 1, 2 is swapped with 1, 4 in the first swap, but the second swap tries to swap 1, 4
        this.lines.delete(swap1.old);
        this.lines.delete(swap2.old);
        this.lines.set(swap1.new, swap1.line);
        this.lines.set(swap2.new, swap2.line);
        this.path = path;
        this.graph.setPath(path);

        if (promises.length > 0) {
            Promise.all(promises).then(() => {
                this.onUpdatePath();
                this.nextIteration();
            });
        } else {
            this.nextIteration();
        }
    }

    private nextIteration() {
        if (this.tries > this.maxTries) {
            this.onFinished();
            return;
        }

        let bestDistance = this.graph.pathDistance(this.path);
        for (let i = this.iCurrent; i < this.graph.numPoints() - 1; i++) {
            for (let k = this.kCurrent; k < this.graph.numPoints(); k++) {
                let newPath = this.swap(i, k);
                let newDistance = this.graph.pathDistance(newPath);
                this.tries++;
                if (newDistance < bestDistance) {
                    this.tries = 0; // Reset tries once an improvement is found
                    bestDistance = newDistance;
                    this.iCurrent = i;
                    this.kCurrent = k;
                    return this.swapTransition(newPath);
                }
            }
            this.kCurrent = i + 1;
        }
        this.iCurrent = 0;
        this.nextIteration();
    }

    /**
     * solve : Initialize path that has already been created using a construction algorithm.
     * -> nextIteration : Swap edges in the tour until a tour with a lower cost is found.
     * -> swapTransition : Perform swap animation.
     * -> nextIteration : Repeat until no better tour is found.
     */
    solve() {
        this.init();

        this.path = this.graph.getPath();
        for (let i = 0; i < this.path.length - 1; i++) {
            let line = new Line(new Connection(this.path[i], this.path[i + 1], this.graph));
            line.setDelay(this.delay);
            this.lines.set(line.connection.toString(), line);
        }
        let line = new Line(new Connection(this.path[0], this.path[this.path.length - 1], this.graph));
        line.setDelay(this.delay);
        this.lines.set(line.connection.toString(), line);

        this.nextIteration();
    }

    init() {
        this.stop = false;
        this.graph.clearLines();
        this.path = [];
        this.lines = new Map<string, Line>();
        this.tries = 0;
        this.iCurrent = 0;
        this.kCurrent = 0;
    }

    destruct() {
        this.stop = true;
    }
}

/**
 * Representation of two points that form a line.
 */
export class Connection {
    public a: Coordinate;
    public b: Coordinate;

    constructor(a: Coordinate | number, b: Coordinate | number, public graph: Graph) {
        this.a = (typeof a == "number") ? graph.getPoint(a) : a;
        this.b = (typeof b == "number") ? graph.getPoint(b) : b;
        // Order is ignored, sort coordinates so that a < b
        if (this.a.compare(this.b) > 0) {
            [this.a, this.b] = [this.b, this.a];
        }
    }

    equals(other: Connection) : boolean {
        return (this.a.equals(other.a) && this.b.equals(other.b));
    }

    toString() : string {
        return `${this.a.toString()}, ${this.b.toString()}`
    }
}

export class Line {
    private readonly line : SVGLineElement;
    private delay: number = 500; // Transition time in milliseconds.
    private other: Connection; // The connection to pivot to

    private from: Coordinate; // Starting point of the pivot
    private to: Coordinate; // Ending point of the pivot
    private start: number;
    private side: string; // Which side should be pivoted, (x1, y1) or (x2, y2)

    constructor(public connection: Connection) {
        this.line = Tools.createLine("black", "2");
        this.setDelay(this.delay);
        connection.graph.getGroup("path").append(this.line);

        this.draw(connection);
    }

    draw(connection: Connection) {
        let a = connection.a;
        let b = connection.b;
        Tools.moveLine(this.line, a.x, a.y, b.x, b.y);
    }

    setDelay(delay: number) {
        this.delay = delay;
    }

    /**
     * Animate the transition from the original connection to the other connection.
     */
    private pivotTransition() : Promise<void> {
        return new Promise((resolve) => {
            let frame = () => {
                let percent = Tools.ease(Math.min((Date.now() - this.start) / this.delay, 1));
                let x = (this.to.x - this.from.x) * percent + this.from.x;
                let y = (this.to.y - this.from.y) * percent + this.from.y;
                this.line.setAttributeNS(null, "x" + this.side, x.toString());
                this.line.setAttributeNS(null, "y" + this.side, y.toString());

                if (Date.now() - this.start < this.delay) {
                    requestAnimationFrame(() => frame());
                } else {
                    this.draw(this.other);
                    this.connection = this.other;
                    resolve();
                }
            };

            frame();
        });
    }

    /**
     * Pivot the line to a new connection. Connection must share one point in common
     * with the original connection, which will be used as the pivot point.
     * @param other Connection to pivot to
     */
    pivot(other: Connection) : Promise<void> {
        // Determine which side is the pivot point
        // Point a is side (x1, y1), point b is side (x2, y2)
        if (this.connection.a.equals(other.a)) {
            this.from = this.connection.b;
            this.to = other.b;
            this.side = "2";
        } else if (this.connection.a.equals(other.b)) {
            this.from = this.connection.b;
            this.to = other.a;
            this.side = "2";
        } else if (this.connection.b.equals(other.a)) {
            this.from = this.connection.a;
            this.to = other.b;
            this.side = "1";
        } else if (this.connection.b.equals(other.b)) {
            this.from = this.connection.a;
            this.to = other.a;
            this.side = "1";
        } else {
            throw "Invalid swap";
        }

        this.start = Date.now();
        this.other = other;
        return this.pivotTransition();
    }
}