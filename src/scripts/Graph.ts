import {Point} from "./Point";

export class Graph {
    private points: Point[] = [];
    private path: number[] = [];
    private readonly pathGroup: SVGGElement;
    private readonly groups: Map<string, SVGGElement>;
    private readonly pointGroup: SVGGElement;
    public readonly canvasContext: CanvasRenderingContext2D;
    private pointRadius: number = 5;

    constructor(public canvas: HTMLCanvasElement, public svg: SVGSVGElement) {
        this.canvasContext = canvas.getContext("2d");

        this.pathGroup = document.createElementNS("http://www.w3.org/2000/svg", "g");
        this.pathGroup.classList.add("pathGroup");

        this.pointGroup = document.createElementNS("http://www.w3.org/2000/svg", "g");
        this.pointGroup.classList.add("nodeGroup");

        svg.append(this.pathGroup);
        svg.append(this.pointGroup);
        this.groups = new Map<string, SVGGElement>();
        this.groups.set("path", this.pathGroup);
        this.groups.set("points", this.pointGroup);

        this.addPointOnClick();
    }

    addOrGetGroup(name: string): SVGGElement {
        if (this.groups.has(name)) return this.getGroup(name);

        let group = <SVGGElement>document.createElementNS("http://www.w3.org/2000/svg", "g");
        this.groups.set(name, group);
        this.svg.append(group);

        return group;
    }

    getGroup(name: string): SVGGElement {
        return this.groups.get(name);
    }

    removeGroup(name: string): boolean {
        // Path and point group cannot be removed
        if ((name != "path" && name != "points") && this.groups.has(name)) {
            let group = this.groups.get(name);
            this.svg.removeChild(group);
            this.groups.delete(name);

            return true;
        }

        return false;
    }

    addPoint(x: number, y: number) {
        this.points.push(new Point(x, y, this.pointRadius, this));
    }

    getPoint(index: number): Point {
        return this.points[index];
    }

    numPoints(): number {
        return this.points.length;
    }

    setPath(path: number[]) {
        this.path = path;
    }

    getPath(): number[] {
        return this.path;
    }

    setPointRadius(radius: number) {
        this.pointRadius = radius;
        this.points.forEach(point => point.setRadius(radius));
    }

    getPointRadius(): number {
        return this.pointRadius;
    }

    pathDistance(path: number[]): number {
        let distance: number = 0;
        for (let i = 0; i < path.length - 1; i++) {
            distance += this.getDistance(path[i], path[i + 1]);
        }
        distance += this.getDistance(path[0], path[path.length - 1]);
        return distance;
    }

    getDistance(a: number, b: number): number {
        return this.points[a].getDistance(this.points[b]);
    }

    private addPointOnClick() {
        let startCoords;
        let click = true;
        let that = this;
        // Disable click event if mouse moves past certain threshold.
        // Begin listening on mousedown and end listening on mouseup.
        let moveFunc = function (event) {
            if (Math.abs(event.clientX - startCoords.x) > 5 ||
                Math.abs(event.clientY - startCoords.y) > 5) {
                click = false;
            }
        };
        this.svg.addEventListener("mousedown", function (event) {
            startCoords = {x: event.clientX, y: event.clientY};
            click = true;
            that.svg.addEventListener("mousemove", moveFunc);
        });
        this.svg.addEventListener("mouseup", function (event) {
            if (click) {
                let coords = that.getMousePosition(event);
                that.addPoint(coords.x, coords.y);
            }
            that.svg.removeEventListener("mousemove", moveFunc);
        });
    }

    generateRandom(num: number) {
        this.clearAll();
        let width = this.svg.viewBox.animVal.width * 0.90;
        let height = this.svg.viewBox.animVal.height * 0.90;

        let side = this.pointRadius * 4;

        let vertical = height / side;
        let horizontal = width / side;

        let points: number[][] = [];

        for (let y = 0; y < vertical; y++) {
            for (let x = 0; x < horizontal; x++) {
                points.push([x, y]);
            }
        }

        // https://stackoverflow.com/a/6274381
        let j, x, i;
        for (i = points.length - 1; i > 0; i--) {
            j = Math.floor(Math.random() * (i + 1));
            x = points[i];
            points[i] = points[j];
            points[j] = x;
        }

        for (let i = 0; i < num; i++) {
            let x = this.svg.viewBox.animVal.height * 0.05 + points[i][0] * side + (Math.random() - 1) * (side - side / 3);
            let y = this.svg.viewBox.animVal.width * 0.05 + points[i][1] * side + (Math.random() - 1) * (side - side / 3);

            this.addPoint(x, y);
        }
    }

    /**
     * Convert mouse screen coordinates to SVG coordinates.
     * @param event Mouse event
     * http://www.petercollingridge.co.uk/tutorials/svg/interactive/dragging/
     */
    getMousePosition(event) {
        let CTM = this.svg.getScreenCTM();
        return {
            x: (event.clientX - CTM.e) / CTM.a,
            y: (event.clientY - CTM.f) / CTM.d
        };
    }

    clearLines() {
        while (this.pathGroup.firstChild) {
            this.pathGroup.removeChild(this.pathGroup.firstChild);
        }
    }

    clearPoints() {
        this.path = [];
        this.points = [];
        while (this.pointGroup.firstChild) {
            this.pointGroup.removeChild(this.pointGroup.firstChild);
        }
    }

    clearCanvas() {
        this.canvasContext.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }

    clearGroup(name: string) {
        let group = this.groups.get(name);
        while (group.firstChild) {
            group.removeChild(group.firstChild);
        }
    }

    clearGroups() {
        for (let name of this.groups.keys()) {
            this.clearGroup(name);
        }
    }

    clearAll() {
        this.clearLines();
        this.clearPoints();
        this.clearCanvas();
        this.clearGroups();
    }
}
