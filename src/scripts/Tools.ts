export class Tools {
    /**
     * Order a and b so that a is smaller than b.
     *
     * Used for accessing the edges in the 2d array. Since the graph is undirected only
     * one sequence of access is allowed. E.g. the edge a -> b is defined, but the edge
     * b -> a is not, so reorder so the access is a -> b.
     * @param a Index of a point in the array
     * @param b Index of a point in the array
     */
    static order(a: number, b: number) : number[] {
        if (a > b) {
            return [b, a];
        }
        return [a, b];
    }

    /**
     * Easing animation based on percentage completed.
     */
    static ease(percent: number) : number {
        return (percent*percent)/(percent*percent + (1 - percent) * (1 - percent));
    }

    static moveLine(line: SVGLineElement, x1: number, y1: number, x2: number, y2: number) {
        line.setAttributeNS(null, "x1", x1.toString());
        line.setAttributeNS(null, "y1", y1.toString());
        line.setAttributeNS(null, "x2", x2.toString());
        line.setAttributeNS(null, "y2", y2.toString());
    }

    static createLine(stroke: string, stroke_width: string) {
        let line = document.createElementNS("http://www.w3.org/2000/svg", "line");
        line.setAttributeNS(null, "stroke", stroke);
        line.setAttributeNS(null, "stroke-width", stroke_width);

        return line
    }

    /**
     * Modulo function with proper negative number behavior.
     */
    static mod(n: number, m: number) : number {
        return ((n % m) + m) % m;
    }
}
