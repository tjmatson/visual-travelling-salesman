import {AntColony} from "./AntColony";

export class Ant {
    trail : number[] = []; // Indices of points in path taken.
    probabilities : number[]; // Probability of visiting point at index.
    current : number; // Index of current point.

    constructor(public colony : AntColony) {
        this.probabilities = Array(colony.graph.numPoints());
    }

    calculateProbabilities() {
        let denominator = 0;
        for (let point = 0; point < this.colony.graph.numPoints(); point++) {
            if (this.trail.indexOf(point) === -1) { // If point hasn't been visited
                let pheromones = Math.pow(this.colony.getEdge(this.current, point), this.colony.alpha) *
                    Math.pow(1 / this.colony.graph.getDistance(this.current, point), this.colony.beta);
                denominator += pheromones;
                this.probabilities[point] = pheromones;
            } else {
                this.probabilities[point] = 0;
            }
        }
        this.probabilities = this.probabilities.map(p => p / denominator);
    }

    nextPoint() : number {
        // Start at a random point.
        if (this.current === null) {
            return Math.floor(Math.random() * this.colony.graph.numPoints());
        }
        // Pick a point based on probabilities.
        this.calculateProbabilities();
        let r = Math.random();
        let total = 0;
        for (let i = 0; i < this.probabilities.length; i++) {
            total += this.probabilities[i];
            if (total >= r) {
                return i;
            }
        }
        throw "No points left";
    }

    visitPoint() : number {
        let point = this.nextPoint();
        this.trail.push(point);
        this.current = point;
        return point;
    }

    forgetTrail() {
        this.trail = [];
        this.current = null;
    }
}

