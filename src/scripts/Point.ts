import {Graph} from "./Graph";

export class Coordinate {
    constructor(public x: number, public y: number) {
    }

    equals(coords: Coordinate): boolean {
        return (this.x == coords.x) && (this.y == coords.y);
    }

    compare(coords: Coordinate): number {
        if (this.equals(coords)) {
            return 0;
        } else if (this.x < coords.x) {
            return -1;
        } else if (this.x > coords.x) {
            return 1;
        } else {
            return (this.y < coords.y) ? -1 : 1;
        }
    }

    toString(): string {
        return `{${this.x}, ${this.y}}`;
    }
}

export class Point extends Coordinate {
    private readonly element: SVGCircleElement;

    constructor(x: number, y: number, public radius: number, private graph: Graph) {
        super(x, y);
        [this.x, this.y] = this.bound(x, y);
        this.element = document.createElementNS("http://www.w3.org/2000/svg", "circle");
        this.element.setAttributeNS(null, "cx", this.x.toString());
        this.element.setAttributeNS(null, "cy", this.y.toString());

        // Pop-in point
        this.element.setAttributeNS(null, "r", "0");
        this.element.style.transition = "0.3s";
        requestAnimationFrame(() =>
            requestAnimationFrame(() => this.element.setAttributeNS(null, "r", this.radius.toString()))
        );

        graph.getGroup("points").append(this.element);
        this.makeDraggable();
    }

    private bound(x: number, y: number) {
        if (x - this.radius < 0) {
            x = this.radius;
        } else if (x + this.radius > this.graph.svg.viewBox.animVal.width) {
            x = this.graph.svg.viewBox.animVal.width - this.radius;
        }
        if (y - this.radius < 0) {
            y = this.radius;
        } else if (y + this.radius > this.graph.svg.viewBox.animVal.height) {
            y = this.graph.svg.viewBox.animVal.height - this.radius;
        }
        return [x, y];
    }

    setRadius(r: number) {
        this.element.setAttributeNS(null, "r", r.toString());
        this.radius = r;
    }

    move(x: number, y: number) {
        [this.x, this.y] = this.bound(x, y);

        this.element.setAttributeNS(null, "cx", this.x.toString());
        this.element.setAttributeNS(null, "cy", this.y.toString());
    }

    getDistance(point: Point): number {
        let a = Math.abs(this.x - point.x);
        let b = Math.abs(this.y - point.y);

        return Math.sqrt(a * a + b * b);
    };

    private makeDraggable() {
        this.element.addEventListener("mousedown", start);

        let selected = false;
        let that = this;
        let offset;

        function start(event) {
            selected = true;

            offset = that.graph.getMousePosition(event);
            offset.x -= parseFloat(that.element.getAttributeNS(null, "cx"));
            offset.y -= parseFloat(that.element.getAttributeNS(null, "cy"));

            // Attach events to the SVG element since the mouse can momentarily leave the point element while dragging.
            that.graph.svg.addEventListener("mousemove", drag);
            that.graph.svg.addEventListener("mouseup", end);
            that.graph.svg.addEventListener("mouseleave", end);
        }

        function drag(event) {
            if (selected) {
                event.preventDefault();
                let coords = that.graph.getMousePosition(event);
                that.move(coords.x - offset.x, coords.y - offset.y);
            }
        }

        function end() {
            selected = false;
            // Remove event listeners after they aren't needed.
            that.graph.svg.removeEventListener("mousemove", drag);
            that.graph.svg.removeEventListener("mouseup", end);
            that.graph.svg.removeEventListener("mouseleave", end);
        }
    }
}